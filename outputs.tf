output "id" {
    value = azurerm_virtual_network.main.id
}

output "name" {
    value = azurerm_virtual_network.main.name
}

output "address" {
    value = azurerm_virtual_network.main.address_space
}