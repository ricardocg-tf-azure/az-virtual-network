terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "2.20.0"
    }
  }
}

provider "azurerm" {
    features {}
    subscription_id = "50505e05-cd97-4b8d-a080-40b5b880ce44"
    tenant_id = "34f55fb5-3a46-4626-b706-5d94d061b0d4"
}

resource "azurerm_virtual_network" "main" {
    name                = var.name
    address_space       = var.address
    location            = var.location
    resource_group_name = var.rg_name

    tags = {
        environment = var.env
    }

    #subnet = {
    #    name           = "subnet2"
    #    address_prefix = "10.0.2.0/24"
    #}
}
