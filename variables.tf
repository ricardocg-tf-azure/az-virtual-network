variable "name" {
    description         = "Name for the VN"
}

variable "address" {
    description         = "IP address space"
}

variable "location" {
    description         = "Location for the VN"
}

variable "rg_name" {
    description         = "Resource Group name"
}

variable "env" {
    description         = "Environment for the VN"
    default             = "test"
}